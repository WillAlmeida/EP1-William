#include "objetosjogo.hpp"

ObjetosJogo::ObjetosJogo(){}

ObjetosJogo::ObjetosJogo(char simbolo, int cx, int cy){
	defCx(cx);
	defCy(cy);
	defSimbolo(simbolo);
}

void ObjetosJogo::defCx(int var){

	this->cx += var;

}

void ObjetosJogo::defCy(int var){
	this->cy += var;
}

void ObjetosJogo::defSimbolo(char simbolo){
	this->simbolo = simbolo;
}

int ObjetosJogo::pegarCx(){
	return this->cx;
}

int ObjetosJogo::pegarCy(){
	return this->cy;
}

char ObjetosJogo::pegarSimbolo(){
	return this->simbolo;
}
