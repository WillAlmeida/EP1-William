#include <iostream>
#include <string>
#include <stdlib.h>
#include "colisoes.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include "jogador.hpp"
#include "mapa.hpp"
#include "armadilha.hpp"
#include "bonus.hpp"

using namespace std;

Colisao::Colisao(){}

void Colisao::ColisaoJP(Jogador *j, Mapa *mapa){

	char tecla;
	tecla = getch();

	if( tecla == 'w' && mapa->detectarSim(j->pegarCy()-1, j->pegarCx()) != '='){

	    j->defCy(-1);

	}

	else if( tecla == 's' && mapa->detectarSim(j->pegarCy()+1, j->pegarCx()) != '='){

	    j->defCy(1);

	}

	else if( tecla == 'd' && mapa->detectarSim(j->pegarCy(), j->pegarCx()+1) != '='){

	    j->defCx(1);

	}

	else if( tecla == 'a' && mapa->detectarSim(j->pegarCy(), j->pegarCx()-1) != '='){

	    j->defCx(-1);

	}

}

void Colisao::ColisaoJA(Jogador *j, Armadilha *a){

	if(a->pegarEstado() == 1){
		if( j->pegarCy() == a->pegarCy() && j->pegarCx() == a->pegarCx()){
				j->defVida(-1);
				a->defEstado(0);
		}
	}
}

void Colisao::ColisaoJB(Jogador *j, Bonus *b){

	if(b->pegarEstado() == 1){
		if( j->pegarCy() == b->pegarCy() && j->pegarCx() == b->pegarCx()){
			j->defPontos(1);
			b->defEstado(0);
		}
	}
}
