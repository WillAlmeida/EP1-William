#include <iostream>
#include <string>
#include "mapa.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>
#include "objetosjogo.hpp"

using namespace std;

Mapa::Mapa(){}

void Mapa::defMapa(){

	ifstream map ("mapa.txt");


	string aux;

	for(int x = 0; x < 20; x++){
	    getline(map, aux);
		for(int y = 0; y < 50; y++){	// Estrutura de repetição para pegar os caracteres do arquivo txt
		    this->dimensao[x][y] = aux[y];
		    this->auxmapa[x][y] = aux[y];
		}
	}

	map.close();
}

void Mapa::printarMapa(){

	for(int x = 0; x < 20; x++){
		for(int y = 0; y < 50; y++){          // Estrutura de repeticao para printar os caracteres na tela
		    printw("%c", this->dimensao[x][y]);
		    this->dimensao[x][y] = this->auxmapa[x][y];
		    if(y >= 49){  // Ajustar as linhas para ficarem separadas
			printw("\n");
		    }
		}

	}

}

void Mapa::addObjeto(char simbolo, int cx, int cy, int estado){
	if(estado == 1){
		this->dimensao[cy][cx] = simbolo;
	}
}


char Mapa::detectarSim(int cy, int cx){
	char simbolo;

	simbolo = this->dimensao[cy][cx];

	return simbolo;

}
