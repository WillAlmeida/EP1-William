#include <iostream>
#include <string>
#include "jogador.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include "objetosjogo.hpp"
using namespace std;

Jogador::Jogador(){}


Jogador::Jogador(char simbolo, int cx, int cy, int vida, int pontos){
	this->simbolo = simbolo;
	this->cx = cx;
	this->cy = cy;
	this->vida = vida;
	this->pontos = pontos;
}



void Jogador::defPontos(int var){
	this->pontos += var;
}

int Jogador::pegarPontos(){
	return this->pontos;
}

void Jogador::defVida(int var){
	this->vida += var;
}

int Jogador::pegarVida(){
	return this->vida;
}
