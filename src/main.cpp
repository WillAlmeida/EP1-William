#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <vector>
#include <stdio_ext.h>
#include "mapa.hpp"
#include "objetosjogo.hpp"
#include "jogador.hpp"
#include "armadilha.hpp"
#include "bonus.hpp"
#include "draw.hpp"
#include "colisoes.hpp"
#include "ranking.hpp"

using namespace std;

int main(){

	WINDOW * janela;

	Mapa * mapa = new Mapa();
	mapa->defMapa();

	Colisao * colisao = new Colisao();
	Ranking rank;
	Draw * draw = new Draw();

	Jogador * jogador = new Jogador('@', 1, 2, 3, 0);

	Armadilha * a1 = new Armadilha('#', 14, 12, 1);
	Armadilha * a2 = new Armadilha('#', 3, 8, 1);
	Armadilha * a3 = new Armadilha('#', 30, 11, 1);
	Armadilha * a4 = new Armadilha('#', 20, 13, 1);
	Armadilha * a5 = new Armadilha('#', 12, 18, 1);
	Armadilha * a6 = new Armadilha('#', 29, 14, 1);
	Armadilha * a7 = new Armadilha('#', 23, 8, 1);
	Armadilha * a8 = new Armadilha('#', 32, 5, 1);
	Armadilha * a9 = new Armadilha('#', 46, 9, 1);
	Armadilha * a10 = new Armadilha('#', 41, 1, 1);

	Bonus * b1 = new Bonus('$', 11, 12, 1);
	Bonus * b2 = new Bonus('$', 48, 1, 1);
	Bonus * b3 = new Bonus('$', 47, 11, 1);
	Bonus * b4 = new Bonus('$', 43, 15, 1);
	Bonus * b5 = new Bonus('$', 16, 3, 1);


	int vivo = 1;

	while(jogador->pegarVida() > 0){
		janela = initscr();
		start_color();
		init_pair(1, COLOR_WHITE, COLOR_BLACK);
		wbkgd(janela, COLOR_PAIR(1));
		clear();
		keypad(stdscr, TRUE);
		noecho();

		mapa->addObjeto(a1->pegarSimbolo(), a1->pegarCx(), a1->pegarCy(), a1->pegarEstado());
		mapa->addObjeto(a2->pegarSimbolo(), a2->pegarCx(), a2->pegarCy(), a2->pegarEstado());
		mapa->addObjeto(a3->pegarSimbolo(), a3->pegarCx(), a3->pegarCy(), a3->pegarEstado());
		mapa->addObjeto(a4->pegarSimbolo(), a4->pegarCx(), a4->pegarCy(), a4->pegarEstado());
		mapa->addObjeto(a5->pegarSimbolo(), a5->pegarCx(), a5->pegarCy(), a5->pegarEstado());
		mapa->addObjeto(a6->pegarSimbolo(), a6->pegarCx(), a6->pegarCy(), a6->pegarEstado());
		mapa->addObjeto(a7->pegarSimbolo(), a7->pegarCx(), a7->pegarCy(), a7->pegarEstado());
		mapa->addObjeto(a8->pegarSimbolo(), a8->pegarCx(), a8->pegarCy(), a8->pegarEstado());
		mapa->addObjeto(a9->pegarSimbolo(), a9->pegarCx(), a9->pegarCy(), a9->pegarEstado());
		mapa->addObjeto(a10->pegarSimbolo(), a10->pegarCx(), a10->pegarCy(), a10->pegarEstado());

		mapa->addObjeto(b1->pegarSimbolo(), b1->pegarCx(), b1->pegarCy(), b1->pegarEstado());
		mapa->addObjeto(b2->pegarSimbolo(), b2->pegarCx(), b2->pegarCy(), b2->pegarEstado());
		mapa->addObjeto(b3->pegarSimbolo(), b3->pegarCx(), b3->pegarCy(), b3->pegarEstado());
		mapa->addObjeto(b4->pegarSimbolo(), b4->pegarCx(), b4->pegarCy(), b4->pegarEstado());
		mapa->addObjeto(b5->pegarSimbolo(), b5->pegarCx(), b5->pegarCy(), b5->pegarEstado());


		mapa->addObjeto(jogador->pegarSimbolo(), jogador->pegarCx(), jogador->pegarCy(), vivo );

		draw->drawMapa();
		printw("Vida:%d \t\t\t Pontos:%d\n", jogador->pegarVida() , jogador->pegarPontos());

		colisao->ColisaoJP(jogador, mapa);

		colisao->ColisaoJA(jogador, a1);
		colisao->ColisaoJA(jogador, a2);
		colisao->ColisaoJA(jogador, a3);
		colisao->ColisaoJA(jogador, a4);
		colisao->ColisaoJA(jogador, a5);
		colisao->ColisaoJA(jogador, a6);
		colisao->ColisaoJA(jogador, a7);
		colisao->ColisaoJA(jogador, a8);
		colisao->ColisaoJA(jogador, a9);
		colisao->ColisaoJA(jogador, a10);


		colisao->ColisaoJB(jogador, b1);
		colisao->ColisaoJB(jogador, b2);
		colisao->ColisaoJB(jogador, b3);
		colisao->ColisaoJB(jogador, b4);
		colisao->ColisaoJB(jogador, b5);



		if (mapa->detectarSim(jogador->pegarCy(), jogador->pegarCx()) == '8'){
			break;
		}

		refresh();
		endwin();
	}

	//Quando ganhar
	clear();
	endwin();

	if(jogador->pegarVida() > 0){
	printf("Você ganhou com %d pontos \n", jogador->pegarPontos());
	}
	//Quando perder
	if(jogador->pegarVida() == 0){
	printf("Você perdeu \n");
	}

string nome;
string pontuacao;

rank.mostrarRank(nome, jogador);       //Mostrar ao usuáro o ranking atual
rank.inserirRank(pontuacao);      //Inserir ganhadores no ranking

	return 0;
}
