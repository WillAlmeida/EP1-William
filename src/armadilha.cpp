#include <iostream>
#include <string>
#include "armadilha.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include "objetosjogo.hpp"

using namespace std;

Armadilha::Armadilha(){}

Armadilha::Armadilha(char simbolo, int cx, int cy, int estado){
	this->simbolo = simbolo;
	this->cx = cx;
	this->cy = cy;
	this->estado = estado;
}

void Armadilha::defEstado(int estado){
	this->estado = estado;
}

int Armadilha::pegarEstado(){
	return this->estado;
}
