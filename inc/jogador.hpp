#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include  "objetosjogo.hpp"

class Jogador: public ObjetosJogo {

private:

	int vida;
	int pontos;

public:

	Jogador();
	Jogador(char simbolo, int cx, int cy, int vida, int pontos);

	int pegarPontos();
	void defPontos(int pontos);
	int pegarVida();
	void defVida(int vida);

};

#endif
