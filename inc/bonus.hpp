#ifndef BONUS_HPP
#define BONUS_HPP

#include "objetosjogo.hpp"

class Bonus : public ObjetosJogo{

private:
	int estado;  			//Ativado ou desativado

public:
	Bonus();
	Bonus(char simbolo, int cx, int cy, int estado);

	void defEstado(int estado);
	int pegarEstado();

};

#endif
