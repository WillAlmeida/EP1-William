#ifndef COLISOES_HPP
#define COLISOES_HPP

#include <iostream>
#include <string>
#include "mapa.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include "objetosjogo.hpp"
#include "jogador.hpp"
#include "armadilha.hpp"
#include "bonus.hpp"

class Colisao {

public:
	Colisao();

	void ColisaoJP(Jogador *j, Mapa *m); //Colisao Jogador-Parede
	void ColisaoJA(Jogador *j, Armadilha *a); //Colisao Jogador-Armadilha
	void ColisaoJB(Jogador *j, Bonus *b); //Colisao Jogador-Bonus

};

#endif
