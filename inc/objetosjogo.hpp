#ifndef OBJETOSJOGO_HPP
#define OBJETOSJOGO_HPP

#include <iostream>
#include <string>

class ObjetosJogo{

protected:

	char simbolo;
	int cx;           // Coordenada X
	int cy;           //Coordenada Y

public:

	ObjetosJogo();
	ObjetosJogo(char simbolo, int cx, int cy);

	int  pegarCx();
	void defCx(int cx);        //definir coordenada x
	int  pegarCy();
	void defCy(int cy);        //definir coordenada y
	char pegarSimbolo();
	void defSimbolo(char simbolo);   //definir simbolo

};

#endif
