#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>
#include <iostream>
#include "objetosjogo.hpp"

class Mapa {

private:
	char dimensao[20][50];
	char auxmapa[20][50]; // Auxiliar de operacoes com o mapa
public:

	Mapa();

	void defMapa();
	void printarMapa();
	void addObjeto(char simbolo, int cx, int cy, int estado);
	char detectarSim(int cy, int cx);

};

#endif
