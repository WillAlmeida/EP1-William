#ifndef ARMADILHA_HPP
#define ARMADILHA_HPP

#include "objetosjogo.hpp"

class Armadilha : public ObjetosJogo {

private:
	int estado; //Ativada ou desativada

public:
	Armadilha();
	Armadilha(char simbolo, int cx, int cy, int estado);

	void defEstado(int estado);
	int pegarEstado();


};

#endif
