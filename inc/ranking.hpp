#ifndef RANKING_HPP
#define RANKING_HPP

#include <iostream>
#include <fstream>
#include <string>
#include "jogador.hpp"

using namespace std;

class Ranking {
private:
  string nome;
  string pontuacao;

public:
	Ranking();

	void mostrarRank(string nome, Jogador *j);       //Mostrar ao usuáro o ranking atual
  void inserirRank(string pontuacao);      //Inserir ganhadores no ranking

};

#endif
