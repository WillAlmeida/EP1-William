#ifndef DRAW_HPP
#define DRAW_HPP

#include "jogador.hpp"
#include "objetosjogo.hpp"
#include "mapa.hpp"
#include "armadilha.hpp"
#include "bonus.hpp"
#include <ncurses.h>
#include <stdio_ext.h>
#include <iostream>
#include <string>

class Draw {

public:

	Draw();

	void drawMapa();

};

#endif
